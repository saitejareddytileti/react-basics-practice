import React, { Component } from 'react';
import Counter from "./counter";

class Counters extends Component {

    render() { 
        console.log("Counters-rendered");

        const {onReset,counters,onDelete,onIncrement}=this.props;

        return (
        <div>
            <button onClick={onReset} className="btn btn-primary btn-sm m-2">Reset</button>
            {counters.map(counter=>
            <Counter key={counter.id} onIncrement={onIncrement} onDelete={onDelete} counter={counter}/>)}
        </div>
        );
    }
}
 
export default Counters;

//Notes:

//key is a special attribute that wont be added into props it is only for uniquely identifying elements

//Each component by default will have selected={true} so we need not mention it

//----------------------------------------------
//Children attr

//If we want to pass complex HTML tags to a child component we can simply write the content inside Counter as <Counter>...content....</Counter> 
//and this will be passed along with props as a property to each of the children elements

//-----------------------------------------------
//The component that owns a piece of state should be the one modifying it

//-----------------------------------------------
//SINGLE SOURCE OF TRUTH
//Even if we update the state of the parent, the props will be updated but state may not because it is disconnected from the value property
//Updating the state of the parent won't update the children's states because they are local and only executed once at the time of creating the instance of the class
//To achieve single source of truth we need to remove the local state and change the state based on its parent