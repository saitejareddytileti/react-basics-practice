import React, { Component } from 'react';

class Counter extends Component {
    componentDidUpdate(prevProps,prevState){
        //This method is called after a component is updated which we have new State and props which we caan compare
        //If there is a change then maybe we can make an AJAX request and retrieve new data
        console.log('prevProps',prevProps);
        console.log('prevState',prevState);
        //we will get state object and we can write some conditions and decide whether we want to make AJAX calls or not

    }

    componentWillUnmount(){
        console.log("Counter-unmount");
        //calls this function before removing it from the DOM
    }

    render() { 
        console.log("Counter-rendered");
        return (
        <div>
            <span className={this.getBadgeClasses()}>{this.formatCount()}</span>
            <button onClick={()=>this.props.onIncrement(this.props.counter)} className="btn btn-secondary btn-sm">Increment</button>
            <button onClick={()=>this.props.onDelete(this.props.counter.id)} className="btn btn-danger btn-sm m-2">Delete</button>
        </div>
        );
    }

    getBadgeClasses(){
        let classes="badge m-2 badge-";
        classes+=(this.props.counter.value===0)?'warning':'primary';
        return classes;
    }

    formatCount(){
        const {value}=this.props.counter;
        return value===0?'Zero':value;
    }
}
 
export default Counter;

//Notes:

//State
//State is completely isolated and local to that particular component so change in state of a component wont effect other components

//Props
//Props is read-only and is accessible by all components

//--------------------------------------
//true&&'Hi'&&1 we get 1 from conditional rendering

//React.Fragment directly appends into the root without any additional div tag
//return(<React.Fragment>.....</React.Fragment>);

//---------------------------------------
// styles={
//     fontSize:'30px',
//     fontWeight:'bold'
// }

// style={this.styles}

// style={{fontSize:'30px'}} 
//--------------------------------------
//{this.state.tags.length===0 && "Please create a new tag!"} if first one is true second is implemented 
//--------------------------------------

//this can reference different objects based on how its called
//obj.method() then this returns reference to that object
//function() if it is called as a stand alone function without any object reference this returns reference to window obj or undefined

//--------------------------------------
//When we call a constructor of a child class we need to also call the constructor of the parent class using super();

//--------------------------------------
//Using the bind method we can set the value of this and it will return a new instance of that function and in that function "this" is always referencing the current object
//Use of bind method: https://stackoverflow.com/questions/2236747/what-is-the-use-of-the-javascript-bind-method#:~:text=bind()%20lets%20you%20specify,the%20body%20of%20the%20function.&text=The%20bind%20function%20creates%20a,called%20with%20the%20this%20argument%20.

//--------------------------------------
//WAYS TO GET REFERENCE TO THE OBJECT
//1ST METHOD
// constructor(){
//     super();
//     this.handleIncrement=this.handleIncrement.bind(this);
// }

// handleIncrement(){
//     console.log(this);
// }

//2ND METHOD:
//handleIncrement=()=>{commands} 

//In both we methods we pass onClick={handleIncrement}

//--------------------------------------
//this.setState() tells react that we have changed the state and it will compare the previous state with the present one in the virtual DOM
//Based on the change it will only re-render and change that one specific HTML element keeping everything else as it is

//Additional Code:
//state={tags: ['tag1','tag2','tag3']}

// renderTags(){
//     if(this.state.tags.length===0) return <p>There are no tags!</p>
//     return (<ul>
//         {this.state.tags.map((tag,index)=><li key={index}>{tag}</li>)}
//     </ul>)
// }

// { {this.renderTags()}}

//-----------------------------------------
//Controlled Component
//A controlled component doesnt have its own local state it receives all the data via props and raises events to change them

//Local state:
//this piece of code is only executed once when a piece of counter component is created
// state={
//     value: this.props.counter.value,  
// }

// handleIncrement(){
//     // this.state.count++;we are incrementing but react is not aware
//     this.setState({value: this.state.value + 1})//this tells the react we are updating the state 
// }