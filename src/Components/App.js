import React, { Component } from 'react';
import NavBar from "./navbar";
import Counters from "./counters"

class App extends Component {

    state = { 
        counters:[
            {id:1,value:0},
            {id:2,value:0},
            {id:3,value:0},
            {id:4,value:0}
        ]
     }

    constructor(props){
        super(props);//if we dont pass props as an argument then it shows props as undefined
        console.log("App-constructor");
        //this.state=this.props;//In constructor we set the state directly
        // this.setState()//gives error as it can only be called when component is rendered and placed in the DOM

    }

    componentDidMount(){
        //this method is called when the component is rendered into the DOM
        //mainly used to make AJAX calls and get data from the server
        // this.setState({new Data})
        console.log("App-mounted")
    }

    handleIncrement=(counter)=>{
        const counters=[...this.state.counters];//shallow copy 
        const index=counters.indexOf(counter);
        counters[index]={...counter}
        counters[index].value++;
        this.setState({counters}); 
    }

    handleDelete=(counterId)=>{  //it is a good practice to write it like this because functions inside classes are methods so we cannot write handleDelete(counterId)
        const counters=this.state.counters.filter(c=>c.id!==counterId);
        this.setState({counters:counters});
    }

    handleReset=()=>{
        const counters=this.state.counters.map(c=>{
            c.value=0;
            return c;
        })
        this.setState({counters})//if the code is of form a:a then we simply write a
    }

    render() { 
        console.log("App-rendered");
        return (
            <React.Fragment>
                <NavBar totalCounters={this.state.counters.filter(c=>c.value>0).length}/>
                <main className="container">
                    <Counters counters={this.state.counters} onReset={this.handleReset} onIncrement={this.handleIncrement} onDelete={this.handleDelete}/>
                </main>
            </React.Fragment> 
         );
    }
}
 
export default App;

//SPREAD OPERATOR
//The spread operator ( … ) is a convenient way to make a shallow copy of an array or object —when there is no nesting
//Shallow copies duplicate as little as possible. A shallow copy of a collection is a copy of the collection structure, not the elements. With a shallow copy, two collections now share the individual elements.

//------------------------------------------------
//PHASES
//MOUNT PHASE: Instance of a component is created and inserted into the DOM
//There are few methods which we can call on our components in each phase and react will automatically call these methods 
//These are called lifecycle hooks which allow us to hook into certain moments during life cycle of a component and do something

//Mount: constructor render componentDidMount 
//Update: render componentDidUpdate
//UnMount: componentWillUnmount

//--------------------------------
//ORDER
//Constructor called and initial variables initialised
//Rendered as a React element which represents the virtual DOM and then React gets into the virtual DOM and renders it into the actual DOM
//Component Mounted means the component is in the actual DOM and is ready to take data

//----------------------------------
//UPDATE PHASE:
//Entire component tree is rendered in the sense when there is a change a react component is returned which updates the virtual DOM
//React now looks at the new DOM formed from setState and then compares with the old one {this is also the reason why we shouldn't directly change the state cuz then react wont be able to comapre}
//Based on the change it updates the actual DOM

