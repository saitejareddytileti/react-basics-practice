import React from 'react';

const NavBar=({totalCounters})=>{

    console.log('Navbar-rendered');

    return (
        <nav className="navbar navbar-expand-lg navbar-light bg-light">
            <p className="navbar-brand" >Navbar<span className="badge badge-pill badge-secondary m-2">{totalCounters}</span></p>
        </nav>
    );
}
 
export default NavBar;

//Generally we can send state as props from parent to child but here Navbar is not a child of Counters components
//In such cases we keep them in sync by lifting the state up to the parent of both i.e the App component

//-------------------------------------------
//FUNCTIONAL COMPONENTS
//If we have only one render method without any helper functions then we can use a stateless functional components
//short command is typing sfc

//IN STATELESS FUNCTIONAL COMPONENTS WE CAN'T USE LIFECYCLE HOOKS because it is just a function returning some lifecycle hooks
